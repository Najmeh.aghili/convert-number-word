const letters = [
  ['', 'یک', 'دو', 'سه', 'چهار', 'پنج', 'شش', 'هفت', 'هشت', 'نه'],
  [
    ' ده',
    'یازده',
    'دوازده',
    'سیزده',
    'چهارده',
    'پانزده',
    'شانزده',
    'هفده',
    'هجده',
    'نوزده',
    'بیست'
  ],
  ['', '', 'بیست', 'سی', 'چهل', 'پنجاه', 'شصت', 'هفتاد', 'هشتاد', 'نود'],
  [
    '',
    'یکصد',
    'دویست',
    'سیصد',
    'چهارصد',
    'پانصد',
    'ششصد',
    'هفتصد',
    'هشتصد',
    'نهصد'
  ],
  ['', ' هزار', ' میلیون', ' میلیارد', ' بیلیون', ' بیلیارد']
]
export default {
  convertToWord (num) {
    if (parseInt(num, 0) === 0) {
      return ''
    }
    let number = parseInt(num)
    if (number < 10) {
      return letters[0][number]
    }
    if (number <= 20) {
      return letters[1][number - 10]
    }
    if (number < 100) {
      let one = number % 10
      let ten = (number - one) / 10
      if (one > 0) {
        return letters[2][ten] + ' و ' + letters[0][one]
      }
      return letters[2][ten]
    }

    let onee = number % 10
    let hundreds = (number - (number % 100)) / 100
    let tenn = (number - (hundreds * 100 + onee)) / 10
    let res = [letters[3][hundreds]]
    let SecondPart = tenn * 10 + onee

    if (SecondPart > 0) {
      if (SecondPart < 10) {
        res.push(letters[0][SecondPart])
      } else if (SecondPart <= 20) {
        res.push(letters[1][SecondPart - 10])
      } else {
        res.push(letters[2][tenn])

        if (onee > 0) {
          res.push(letters[0][onee])
        }
      }
    }

    return res.join(' و ')
  },

  prepareNumber (num) {
    let Out = num
    if (typeof Out === 'number') {
      Out = Out.toString()
    }
    const NumberLength = Out.length % 3
    if (NumberLength === 1) {
      Out = '00'.concat(Out)
    } else if (NumberLength === 2) {
      Out = '0'.concat(Out)
    }
    const res = Out.replace(/\d{3}(?=\d)/g, '$&*').split('*')
    return res
  },

  convertbig (number) {
    let input = number.toString().replace(/[^0-9.-]/g, '')
    let slicedNumber = this.prepareNumber(input)
    let Output = []
    let SplitLength = slicedNumber.length

    for (let i = 0; i < SplitLength; i += 1) {
      let SectionTitle = letters[4][SplitLength - (i + 1)]
      let converted = this.convertToWord(slicedNumber[i])
      if (converted !== '') {
        Output.push(converted + SectionTitle)
      }
    }
    return Output.join(' و ')
  },

  reverse (x) {
    return x
      .split('')
      .reverse()
      .join('')
  },

  addComma (value) {
    let x = value
    x = x.replace(/,/g, '')
    x = this.reverse(x)
    x = x.replace(/.../g, function (e) {
      return e + ','
    })
    x = this.reverse(x)
    x = x.replace(/^,/, '')

    return x
  }
}
